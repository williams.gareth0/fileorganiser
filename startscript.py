import os
import time
from pathlib import Path
from file_checker import check_files

user = os.environ['USER']
basepath = '/home/%s/Downloads'%user

def read_files():
    for entry in os.listdir(basepath):
        if os.path.isfile(os.path.join(basepath, entry)):
            check_files(entry, basepath)


while True:
    localtime = time.localtime()
    local = time.strftime('%I:%M:%S %p', localtime)

    print('%s: reading downloads directory'%(local))

    read_files()

    time.sleep(60)
