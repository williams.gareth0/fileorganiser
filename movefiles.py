import os
import shutil
import time
from pathlib import Path

def create__dir(file, basepath, folder_name):
    directory = Path(basepath + '/' + folder_name)
    path = basepath + '/' + folder_name
    folderexists = os.path.exists(path)  

    if (not folderexists):
        directory.mkdir()
        move__file(file, basepath, folder_name)
    else:
        move__file(file, basepath, folder_name)

def move__file(file, basepath, folder_name):
    localtime = time.localtime()
    local = time.strftime('%I:%M:%S %p', localtime)

    print('%s: moving %s files to %s directory'%(local, folder_name, folder_name))

    shutil.move(basepath + '/' + file, basepath + '/' + folder_name)