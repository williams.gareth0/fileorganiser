# File Organiser

File Organiser is a Python script for dealing with automatically organising your downloads folder contents for into separate folders according to their file extension (music, images, documents, etc.).

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install package ` pathlib ` for reading and managing your OS paths.

Run the following command in your terminal:

```bash
pip install pathlib
```

## Usage

```python
python startscript.py
```

Adding file extensions can be done in the ` file_checker.py ` file. Current supported extensions are:

```python
videoformats = ['mp4', 'avi', '3pg', 'mkv', 'mov']
imageformats = ['jpeg', 'jpg', 'gif', 'png', 'ico', 'svg', 'tiff', 'webp']
docsformats = ['xlsx', 'pdf', 'csv', 'xls', 'doc', 'docx', 'odt', 'ppt', 'pptx', 'ods', 'odp', 'rtf']
zipformats = ['zip', 'deb', 'tar', 'apk', 'rar']
musicformats = ['mp3', 'wav', 'wma', 'acc', 'ogg']
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)