from movefiles import create__dir

videoformats = ['mp4', 'avi', '3pg', 'mkv', 'mov']
imageformats = ['jpeg', 'jpg', 'gif', 'png', 'ico', 'svg', 'tiff', 'webp']
docsformats = ['xlsx', 'pdf', 'csv', 'xls', 'doc', 'docx', 'odt', 'ppt', 'pptx', 'ods', 'odp', 'rtf']
zipformats = ['zip', 'deb', 'tar', 'apk', 'rar']
musicformats = ['mp3', 'wav', 'wma', 'acc', 'ogg']


def check_files(file, basepath):
    file_format = file.split('.')[1]

    if (file_format in videoformats):
        create__dir(file, basepath, 'videos')
    elif (file_format in imageformats):
        create__dir(file, basepath, 'images')
    elif (file_format in docsformats):
        create__dir(file, basepath, 'documents')
    elif (file_format in zipformats):
        create__dir(file, basepath, 'zip files')
    elif (file_format in musicformats):
        create__dir(file, basepath, 'music')
    else:
        create__dir(file, basepath, 'other')    
    